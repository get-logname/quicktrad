# QuickTrad



**This project is not affiliated with, or endorsed by DeepL GmbH.**

This Firefox addon helps you to get translations in a quick way, it automatically translates the text that you select and place it in your clipboard.

It uses the DeepL Pro API, so you need an active paid developper plan with them. DeepL Pro API billing is usage based so be carrefull with that, if you select a billion caracters by accident, or if the plugin has a bug and selects a billion caracters by accident, DeepL will charge you for it. Use a your own risks, I won't take ANY responsability.



## Usage:

- Clone this repo somewhere on your computer.
- Create a new secret.js file at the root of the cloned repository and place your API key in it (check the secret.js.example file).
- In Firefox open the [about:debugging](about:debugging) page, click "This Firefox", click "Load  Temporary Add-on", then double click on any file in the quicktrad directory.
- Select some text, and you should have the translation ready for you in your clipboard.
- Enjoy :)

