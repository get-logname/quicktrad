function getSelectedText(){

    let selectedText = "";

    if (window.getSelection){
        selectedText = window.getSelection().toString()
    }

    return selectedText
}

function setTranslation(text) {

    fetch(`https://api.deepl.com/v2/translate?auth_key=${API_KEY}&target_lang=FR&text=${text}`)
        .then(response => response.json())
        .then(data => {
            navigator.clipboard.writeText(data.translations[0].text);
        });
}

document.addEventListener('mouseup', function(){

    let selectedText = getSelectedText();

    if (selectedText.length > 2){
        setTranslation(selectedText);
    }

}, false);